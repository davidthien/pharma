import ParsePharma
import CFG
import GenCreator
import ParseToGrammar

import System.Environment
import Data.Map.Strict
import Test.QuickCheck

printStringCFG :: [(String, [String])] -> IO ()
printStringCFG cfg = case cfg of
    (s, r):ls -> do
        putStr $ s ++ ": "
        printRulesCFG r
        printStringCFG ls
    [] -> putChar '\n'

printRulesCFG rules = case rules of
    r1:r2:rs -> do
        putStr $ r1 ++ ", "
        printRulesCFG $ r2:rs
    r1:[] -> do
        putStr r1
        putStr "\n"
    [] -> do putStr "\n"

printPharma :: String -> IO ()
printPharma s = printStringCFG $ parsePharma s

printPharmaFromFile :: String -> IO ()
printPharmaFromFile f = do
    s <- readFile f
    printStringCFG $ parsePharma s

getGrammar :: String -> Grammar
getGrammar s = convertToGrammar $ parsePharma s

printGrammar :: String -> IO ()
printGrammar s = putStrLn $ show $ getGrammar s

getFirstRule :: String -> RuleName
getFirstRule s = RuleName $ parseFirstRuleName s

genFromFile :: String -> IO ()
genFromFile f = do
    s <- readFile f
    let firstRule = getFirstRule s
    let grammar = getGrammar s
    prog <- generateProgram grammar firstRule
    putStrLn prog

main = do
    [f] <- getArgs
    genFromFile f
