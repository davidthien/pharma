module GenContext where

import CFG

import Data.Map.Strict
import Test.QuickCheck

data GenContext = GenContext [(VariableName, Type)] Integer Grammar
    deriving (Show)

newtype Type = Type String
    deriving (Eq, Show)

newtype VariableName = VariableName String
    deriving (Show)

getNextInt :: GenContext -> (GenContext, Integer)
getNextInt con = case con of
    GenContext v i g -> (GenContext v (i + 1) g, i)

getVar :: GenContext -> Gen String
getVar (GenContext vars _ _) = elements (Prelude.map (\(VariableName x, _) -> x) vars)

getVarOfType :: GenContext -> Type -> Gen String
getVarOfType (GenContext vars _ _) t = elements [x | (VariableName x, t) <- vars]

insertVar :: GenContext -> (VariableName, Type) -> GenContext
insertVar (GenContext vars i g) newVar = GenContext (newVar : vars) i g

emptyGenContext :: GenContext
emptyGenContext = GenContext [] 0 emptyGrammar

getGrammar :: GenContext -> Grammar
getGrammar con = do
    let GenContext _ _ g = con
    g

getGrammarMap :: GenContext -> Map RuleName [RuleExpr]
getGrammarMap con = do
    let Grammar g = getGrammar con
    g
