module GenCreator where

import CFG
import GenContext
import DefaultGens

import Test.QuickCheck
import Data.Map.Strict

-- Setting up our context
grammarToGen :: Grammar -> GenContext
grammarToGen grammar = GenContext [] 0 grammar

-- Setting up our final generators; after this is done, we can arbitrarily generate
-- rules from our language using this function's output map
grammarMapToStringGen :: GenContext -> Map RuleName (Gen String)
grammarMapToStringGen con = do
    let g = getGrammarMap con
    let map1 = Data.Map.Strict.map (\x -> elements x) g
    let map2 = Data.Map.Strict.map (\x -> ruleExprToStringGen con x) map1
    let map3 = Data.Map.Strict.map removeContext map2
    map3

removeContext :: Gen (GenContext, String) -> Gen String
removeContext conGen = do
    (_, s) <- conGen
    return s

ruleExprToStringGen :: GenContext -> Gen RuleExpr -> Gen (GenContext, String)
ruleExprToStringGen con ruleGen = do
    (RuleExpr ruleVals quals) <- ruleGen :: Gen RuleExpr
    case ruleVals of
        [] -> return (con, "")
        r:rs -> do
            (newCon, rVal) <- ruleValToGen con r
            (finalCon, next) <- ruleExprToStringGen newCon $ return $ RuleExpr rs quals
            return (finalCon, rVal ++ next)

ruleValToGen :: GenContext -> RuleVal -> Gen (GenContext, String)
ruleValToGen con val = case val of
    Text s -> return (con, s)
    NextRule name -> do
        --context here for looking up types
        let Grammar g = getGrammar con
        let v = Data.Map.Strict.lookup name g
        case v of
            Nothing -> if isDefaultGen name
                then getDefaultGen con name
                else return (con, "") -- Should be a compilation error
            Just exps -> do
                -- case vardef
                ruleExprToStringGen con $ elements exps
    RuleVariable _ name qs -> do
        --context here
        let Grammar g = getGrammar con
        let v = Data.Map.Strict.lookup name g
        case v of
            Nothing -> return (con, "") -- Should be a compilation error
            Just exps -> do
                case containsReqType qs of
                    Nothing -> ruleExprToStringGen con $ elements exps
                    Just reqType -> do
                        let typedExps = getTypedExpList exps reqType
                        ruleExprToStringGen con $ elements typedExps

getTypedExpList :: [RuleExpr] -> Type -> [RuleExpr]
getTypedExpList rules t = case rules of
    [] -> []
    (RuleExpr e quals):rs -> case containsType quals of
        Nothing -> getTypedExpList rs t
        Just ty -> if ty == t
            then (RuleExpr e quals):(getTypedExpList rs t)
            else getTypedExpList rs t

addDefaultGens :: Map RuleName (Gen String) -> Map RuleName (Gen String)
addDefaultGens m = do
    let m1 = Data.Map.Strict.insert (RuleName "randString") randString m
    let m2 = Data.Map.Strict.insert (RuleName "randInt") randInt m1
    m2

generateProgram :: Grammar -> RuleName -> IO String
generateProgram grammar rule = do
    let setupContext = grammarToGen grammar
    let lookupGen = grammarMapToStringGen setupContext
    let completeLookupGen = addDefaultGens lookupGen
    let ruleGen = Data.Map.Strict.lookup rule completeLookupGen
    case ruleGen of
        Nothing -> return "Lookup Failed"
        Just g -> do
            generate g
