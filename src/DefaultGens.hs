module DefaultGens where

import GenContext
import CFG

import Data.List
import Test.QuickCheck

randString :: Gen String
randString = listOf (elements (['a' .. 'z'] ++ ['A' .. 'Z']))

randInt ::  Gen String
randInt = listOf (elements ['0' .. '9'])

var :: GenContext -> Gen String
var con = getVar con

varDef :: GenContext -> Gen (GenContext, String)
varDef con = do
    s <- randString
    return (insertVar con (VariableName s, Type ""), s)

incrementor :: GenContext -> Gen (GenContext, String)
incrementor con = case getNextInt con of
    (c, i) -> return (c, show i)

containsType :: Qualifiers -> Maybe Type
containsType qual = case qual of
        Qualifiers [] -> Nothing
        Qualifiers (q:qs) -> if isInfixOf "type" q
            then if q !! 0 == ' '
                then Just $ Type (drop 6 q)
                else Just $ Type (drop 5 q)
            else containsType (Qualifiers qs)

containsReqType :: Qualifiers -> Maybe Type
containsReqType qual = case qual of
        Qualifiers [] -> Nothing
        Qualifiers (q:qs) -> if isInfixOf "reqtype" q
            then if q !! 0 == ' '
                then Just $ Type (drop 9 q)
                else Just $ Type (drop 8 q)
            else containsReqType (Qualifiers qs)

containsVarDef :: Qualifiers -> Bool
containsVarDef quals = case quals of
    Qualifiers [] -> False
    Qualifiers (q:qs) -> if isInfixOf "vardef" q
        then True
        else containsVarDef (Qualifiers qs)

isDefaultGen :: RuleName -> Bool
isDefaultGen s = case s of
    RuleName str -> elem str ["varDef", "var", "randString", "randInt", "incrementor"]

getDefaultGen :: GenContext -> RuleName -> Gen (GenContext, String)
getDefaultGen con (RuleName name) = case name of
    "varDef" -> varDef con
    "var" -> do
        s <- var con
        return (con, s)
    "randString" -> do
        s <- randString
        return (con, s)
    "randInt" -> do
        s <- randInt
        return (con, s)
    "incrementor" -> incrementor con
