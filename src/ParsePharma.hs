module ParsePharma where
import Data.Text
import Data.List.Extra (dropEnd, trim)

parseFirstRuleName :: String -> String
parseFirstRuleName s = unpack $ Prelude.head $ split (== ' ') (pack s)

parsePharma :: String -> [(String, [String])]
parsePharma input = do
    let newlineClean = replace (pack "\\n") (pack "\n") 
                      (replace (pack "\n") (pack "") (pack input))
    let initialRuleSplit = splitOn (pack ";") newlineClean
    let rules = getRules (Prelude.take ((Prelude.length initialRuleSplit) - 1) initialRuleSplit)
    let textRules = Prelude.map (\(x, y) -> (x, splitRules y)) rules
    Prelude.map (\(x, y) -> (trim$ unpack(x), Prelude.map unpack y)) textRules

splitRules :: Text -> [Text]
splitRules input = splitOn (pack "|") input

getRule :: Text -> (Text, Text)
getRule ruleLine = do
    let name:exprs:_ = splitOn (pack ":=") ruleLine
    (name, exprs)

getRules :: [Text] -> [(Text, Text)]
getRules input = do
    Prelude.map getRule input
