module ParseToGrammar where

import CFG
import ParsePharma

import Data.Map.Strict
import Data.Text
import Data.List

convertToGrammar :: [(String, [String])] -> Grammar
convertToGrammar rules = Grammar $ rulesToMap rules Data.Map.Strict.empty

rulesToMap :: [(String, [String])] -> Map RuleName [RuleExpr] -> Map RuleName [RuleExpr]
rulesToMap rules m = case rules of
    [] -> m
    r:rs -> do
        let (name, ruleExprs) = r
        let nextMap = Data.Map.Strict.insert (RuleName name) (parseRuleLines ruleExprs) m
        rulesToMap rs nextMap

parseRuleLines :: [String] -> [RuleExpr]
parseRuleLines lines = Prelude.map (\x -> do
        let (rules, quals) = parseRuleLine x
        RuleExpr rules quals) lines

parseRuleLine :: String -> ([RuleVal], Qualifiers)
parseRuleLine ruleLine = do
    let parts = splitOn (pack "@") (pack ruleLine)
    case parts of
        expr:quals:[] -> do
            let es = exprToRuleVals expr
            let qs = qualsToRuleVals quals
            let combinedExpr = combineExprQuals es qs
            let ruleQualifier = parseRuleQualifier qs
            (combinedExpr, ruleQualifier)
        expr:[] -> (exprToRuleVals expr, Qualifiers [])
        _ -> ([], Qualifiers [])

parseRuleQualifier :: [RuleVal] -> Qualifiers
parseRuleQualifier exprs = case exprs of
    (RuleVariable (VarName n) _ q):es -> if n == "" then q else (parseRuleQualifier es)
    e:es -> parseRuleQualifier es
    _ -> Qualifiers []

combineExprQuals :: [RuleVal] -> [RuleVal] -> [RuleVal]
combineExprQuals exprs quals = case quals of
    [] -> exprs
    q:qs -> case q of
        RuleVariable varName _ quals -> do
            let m = Data.List.findIndex (\x -> case x of
                    RuleVariable v r q -> v == varName
                    _ -> False) exprs
            case m of
                Nothing -> combineExprQuals exprs qs
                Just i -> do
                    let RuleVariable finalVar finalName _ = exprs !! i
                    let front = Prelude.take i exprs
                    let back = Prelude.drop (i + 1) exprs
                    combineExprQuals (front ++ ((RuleVariable finalVar finalName quals) : back)) qs
        _ -> combineExprQuals exprs qs

exprToRuleVals :: Text -> [RuleVal]
exprToRuleVals t = do
    let (open, close) = breakOn (pack "{") t
    if open /= (pack "")
        then (Text (unpack open)):(exprToRuleVals close)
        else do
            if close == (pack "") -- We are done parsing
                then []
                else do
                    let (var, end) = breakOn (pack "}") (Data.Text.drop 1 close)
                    if close == (pack "") --ERROR
                        then [Text ""]
                        else do
                            (parseRuleVar var):(exprToRuleVals (Data.Text.drop 1 end))

qualsToRuleVals :: Text -> [RuleVal]
qualsToRuleVals qualLine = do
    let parts = splitOn (pack "{") (strip qualLine)
    let qualVars = Prelude.map (\x -> Prelude.head (splitOn (pack "}") x)) (Prelude.drop 1 parts)
    Prelude.map qualExprToRuleVal qualVars

qualExprToRuleVal :: Text -> RuleVal
qualExprToRuleVal t = do
    let list = splitOn (pack ":") t
    case list of
        var:quals:[] -> do
            let qualList = splitOn (pack ", ") quals
            RuleVariable (VarName (unpack var))
                (RuleName "") 
                (Qualifiers $ Prelude.map unpack qualList)
        quals:[] -> do
            let qualList = splitOn (pack ", ") quals
            RuleVariable (VarName "") (RuleName "") (Qualifiers $ Prelude.map unpack qualList)
        _ -> Text "" --ERROR

parseRuleVar :: Text -> RuleVal
parseRuleVar t = do
    let list = splitOn (pack ":") t
    case list of
        var:rule:[] -> RuleVariable 
            (VarName (unpack var)) 
            (RuleName (unpack $ strip rule)) 
            (Qualifiers [])
        rule:[] -> NextRule (RuleName (unpack rule))
        [] -> Text "" --ERROR
