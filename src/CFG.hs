module CFG where

import Data.Map.Strict

data Grammar = Grammar (Map RuleName [RuleExpr])
    deriving (Show)

emptyGrammar :: Grammar
emptyGrammar = Grammar empty

data RuleName 
    = RuleName String 
    deriving (Eq, Ord, Show)

data VarName = VarName String
    deriving (Eq, Show)

data RuleExpr = RuleExpr [RuleVal] Qualifiers
    deriving (Show)

data RuleVal
    = Text String
    | NextRule RuleName
    | RuleVariable VarName RuleName Qualifiers
    deriving (Show)

newtype Qualifiers = Qualifiers [String]
    deriving (Show)
