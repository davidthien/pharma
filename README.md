# Pharma #

Pharma is a DSL for generating test programs from a grammar.

Proposal link: https://docs.google.com/document/d/1Zx9GcxZ1OIp1oVTJn5-LOlzGXDqPgX8agojxwoLC0Ok/edit?usp=sharing

Design Document link: https://docs.google.com/document/d/1XO1GEf5g6YsFL2pvLBU_Uqd4HKOg6X0ziQl5oyKQwkg/edit?usp=sharing

Report link: https://docs.google.com/document/d/1ZK_Zxz5W6HSKZMXBCRtru-hpS1YnNrurhnwpJHkxXAk/edit?usp=sharing

## Installation
Pharma requires the QuickCheck, and Extra libraries

# Tutorial
Pharma is a testing language designed to make generating random programs easy and quick to do. The language is specified in a format similar to CFGs in Bakus-Naur notation.

### First Program
One simple program that we can generate is a simple boolean expression generator.

```
binop :={binop} and {binop}
       |{binop} or {binop}
       |{bool}
       ;

bool :=True|False;
```

You can see in this simple program, we simply define 2 rules. The first rule is `binop` (for binary operation) and it has 3 expressions that it can generate. Note that rules start with the rule name (alpha-numeric characters) followed by the `:=` designating the start of our rule's expressions. Each subsequent experssion is followed by a `|` to indicate the next expression. The rule itself is ended with `;`. By default, Pharma will assume that it should generate the first rule we define in our grammar.

Note as well that we have mixed text elements and calls to other rules. The part of the expression `{binop}` is a rule-referencing element. This means that it will recurse through whatever rule is specified inside the parenthesis. Since these recursive calls are interspersed with regular text, we surround them in braces to distinguish them. 2 expressions in the binop rule call back into itself, and the last one calls into bool, which terminates our string.

Now that we have our grammar file, we can generate a random program from it using pharma. To run this grammar, save it as ex1.cfg and then run it using `runghc Pharma.hs ex1.cfg`. The exact output will be randomized so we don't only generate one program, but it should like something like the examples below.

```
True

True or False

True and False or True or True
```

Note that you have to be careful when defining your grammar not to generate infinite programs, and not to give the generator a good probability of running for essentially infinite time. Currently all expressions for each rule are generated with the same probability, and there is no limiting factor except the one imposed by your computing power. There are plans to add a method to specify the frequency with which each program should be generated in the future, but for now, be careful.

In addition to user defined rules, there are also a few default rules which are included with all pharma programs. These are

- incrementor: counts up every time it is called starting from 0
- randInt: generates a random integer
- randString: generates a random string
- varDef: defines a new variable
- var: references a previously defined variable

We will come back to the last 2 later, but first lets look at an example using the first 3

```
line :={var} = {randInt}\n
      |{line}{line}

var :={randString}{incrementor};
```

This example shows off several features. First off, lets take a look at some example output

```
aiwekkx0 = 129
iefnz1 = 0
ickzx3 = 1230981829

iczklkj0 = 82390
```
We can see from the sample output that this program generates variable-like definitions.
